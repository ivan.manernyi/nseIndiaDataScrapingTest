﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using RandomUserAgent;

namespace nseindiaParsingMarketData
{
    public class Tests
    {
        private IWebDriver _driver;
        private readonly string pathToChrome = @"Chrome"; // Путь к папке Chrome Driver
        private readonly string startTestingURL = @"https://www.nseindia.com/"; 
        private readonly string delimiter = @";"; // Символ разграничения
        private readonly string pathToCSV = @"output.csv"; // Путь к файлу CSV
        private IWebElement mainMenuButton; // Кнопка в главном меню
        private IWebElement secondaryMainMenuButton; // Кнопка в выпадающем меню
        private IWebElement nameCell, totalCell; 
        private bool clearCacheOnStart = true; // Удалять ли cookie перед запуском теста
        
        [SetUp]
        public void Setup()
        {
            ChromeOptions options = new ChromeOptions();
            // Опции для обхода бот-детектора
            options.AddArgument("--disable-extensions");
            options.AddArgument("--profile-directory=Default");
            options.AddArgument("--disable-plugins-discovery");
            options.AddArgument("--start-maximized");
            options.AddArgument("--window-size=1280,800");
            options.AddArgument("--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36");
            options.AddArgument("--disable-blink-features=AutomationControlled");

            _driver = new ChromeDriver(pathToChrome, options);
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
            if (clearCacheOnStart == true)
            {
                ClearBrowserCache(); // Чистим куки
            }
            _driver.Navigate().GoToUrl(startTestingURL);
        }

        [Test]
        public void Test1()
        {
            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(3));

            IJavaScriptExecutor js = (IJavaScriptExecutor) _driver;
            js.ExecuteScript("document.querySelector('#main_navbar > .navbar-nav > .nav-item:nth-child(3) > .dropdown-menu > .container > .row > [class^=col]:nth-child(1) > .nav > .nav-item:nth-child(1) > .nav-link').click()");

            // Формируем пустой список для сохранения в файл CSV и формируем коллекции с элементами
            List<string> outputCSVList = new List<string>(); // Список строк для выходного файла
            ICollection<IWebElement> numberOfElementsName =
                _driver.FindElements(By.CssSelector(".table-wrap #livePreTable tr td a[target]")); // Коллекция, столбец NAME
            ICollection<IWebElement> numberOfElementsFinalPrice =
                _driver.FindElements(By.CssSelector(".table-wrap #livePreTable .bold")); // Коллекция, столбец FINAL PRICE
            // Обьединяем две коллекции
            foreach (var tuple in numberOfElementsName.Zip(numberOfElementsFinalPrice.Skip(1), (x, y) => (x, y)))
            {
                outputCSVList.Add(tuple.x.Text.ToString() + delimiter + tuple.y.Text.ToString());
            }
            System.IO.File.WriteAllLines(pathToCSV, outputCSVList); // Сохраняем CSV файл
            if (File.Exists(pathToCSV))
            {
                _driver.Close();
                _driver.Quit();
            }
        }

        public void ClearBrowserCache()
        {
            _driver.Manage().Cookies.DeleteAllCookies(); // Удаляем Cookies
            Thread.Sleep(7000); // Ждём удаления
        }
    }
}